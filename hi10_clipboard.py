from pyperclip import copy,paste
from re import sub,search,findall,IGNORECASE
from hashlib import md5
from random import randrange
from pprint import pprint
from time import time
from pathlib import Path



## Path for the backup files
BACKUP_PATH=Path('Hi10anime_links')
TEMP_PATH=Path.home()/'Desktop'
## Gets the HTML from clipboard
## Yeah, you have to actually load the site and copy it manually
html=paste()

## Check if its from hi10anime, and get the name of the Anime, else exit
try:
	archive=search(r'<link rel="canonical" href="https://hi10anime.com/archives/(.*?)" \/>',html)[1]
	title=search(r'<h1 class="entry-title">(.*?)<\/h1>',html)[1]
	title=sub(r'[^\w_\.)( \-\[\]]', '', title)
	title=archive+' - '+title
	print(title)
except:
	print(" Hi10anime.com source not found.Please copy again!! ".center(60,'*'))

	try:
		anime_dict=dict(enumerate(BACKUP_PATH.glob('*.txt'),1))
		for i,file in anime_dict.items():
			print(f"{i} --> {file.stem}")
		x=int(input("Press Key to fetch anime from the following list:\n(press 0 to exit) "))
		
		if x<1:
			raise Exception
		import webbrowser
		## TODO: FIX hack
		# chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
		webbrowser.open(f'https://hi10anime.com/archives/{anime_dict[x].stem.split()[0]}')
		input("Press Enter to continue")
		html=paste()
		archive=search(r'<link rel="canonical" href="https://hi10anime.com/archives/(.*?)" \/>',html)[1]
		title=search(r'<h1 class="entry-title">(.*?)<\/h1>',html)[1]
		title=sub(r'[^\w_\.)( \-\[\]]', '', title)
		title=archive+' - '+title
		print(title)
	except Exception as e:
		print(str(e))
		exit()

## TODO: Redundant code - fix this


## Here is were the magic happens and you get all the links
matches=findall(r"=(https://.*?hi10.*?mkv)",html,IGNORECASE)
match_set=sorted(set(matches))
jtoken=lambda :(x:='%05x' % randrange(16**5))+md5(x.encode()).hexdigest()[:5]
output="".join((i+f"?jtoken={jtoken()}\n" for i in match_set))

## Temp create in the Desktop
fpath=Path(f'{title}.txt')
TEMP_PATH/=fpath
TEMP_PATH.write_text(output)

## Save a backup in a the BACKUP_PATH for future use (probably may never use again tho)
# BACKUP_PATH=Path(Path.home()/'Documents'/'Hi10anime_links')/fpath
if not BACKUP_PATH.is_dir():
	BACKUP_PATH.mkdir(exist_ok=True)

# BACKUP_PATH/=fpath
## TODO: If file is present, read and add only those that are new
(BACKUP_PATH/fpath).write_text(output)

## Create a .bat file to run for that particular anime
# def create_bat():
# 	bat_path=BACKUP_PATH/Path(f'{title}.bat')
# 	bat_text="""

# 	"""
# 	bat_path.write_text()


## To see what was found
pprint(match_set)

## This option is to facilitate 'Add batch download from clipboard' in idm
copy(output)

## TODO: Copy the latest episode (MIGHT NOT WORK FOR MOST coz of OP/ED)
# copy(match_set[-1]+f"?jtoken={jtoken()}")

