# Hi10anime Link Extractor

### This is a script that assists in downloading Anime from http://hi10anime.com
---
## Instructions
- Open http://hi10anime.com and login
- Search for the anime you want to download
- Load the required page and View Source `Ctrl+U` of the page
- Select all `Ctrl+A` and Copy `Ctrl+V` the entire source
- Fire the script and it will have all the links in your clipboard
- ENJOI!
---
## PS:
The script will also make a file with the name of the Anime from where you run the file

As well in the `Documents` folder of your home directory
